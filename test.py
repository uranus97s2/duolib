from straightline import StraightLine
from point import Point
from vector import Vector

A = Point(1, 2)
B = Point(4, 5)
C = Point(3, 7)
AB = Vector.create((A, B))
# print(A)
# print(B)
# print(C)
d1 = StraightLine(AB, A)
print(d1)