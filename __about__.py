# This file is dual licensed under the terms of the Apache License, Version
# 2.0, and the BSD License. See the LICENSE file in the root of this repository
# for complete details.

from __future__ import absolute_import, division, print_function

__all__ = [
    "__title__", "__summary__", "__uri__", "__version__", "__author__",
    "__email__", "__license__", "__copyright__",
]

__title__ = "Duolib"
__summary__ = ("Duolib is a package that provides mathematical formulas"
               " and primitives to Python developers.")
__uri__ = "https://gitlab.com/pyduolib/Duolib"

__version__ = "1.0.0"

__author__ = "Duong pham"
__email__ = "phduong1997s@gmail.com"

__license__ = "MIT License"
__copyright__ = "Copyright (c) 2019 {0}".format(__author__)
