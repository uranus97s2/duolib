# How to using class: point 

**initialize with class point**
```
   from point import Point
   
   point = Point(1, 2)
   print(point)
```
Result: Point(1, 2)
****
**other**
```    
    from point import Point
    
    # values can is str, list, tuple, dict and Point

    # with value is str 
    point = Point.create("(1, 2)")
    
    # with value is list
    point = Point.create([1, 2])
    
    # with value is tuple 
    point = Point.create((1, 2))
    
    # with value is dict 
    point = Point.create({"x":1, "y":2})

    # with value is Point
    point = Point.create(Point(1, 2))
```