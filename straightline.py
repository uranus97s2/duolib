from vector import Vector
from  point import Point

class StraightLine():

    def __init__(self, vector, A):
        """ Create a straightline, example:  d = StraightLine(Vector(1,2), Point(2,3) """
        self.vector = Vector.create(vector)
        self.A = Point.create(A)

    def getA(self):
        return self.vector[0]

    def getB(self):
        return self.vector[1]

    def getC(self):
        return -self.vector[0] * self.A.x - self.vector[1] * self.A.y

    def checkdau(self, x):
        dau = {True: '+', False: '-'}
        return dau[x >= 0]

    def det(self, a, b, c, d):
        return a * d - b * c

    def equations(self, pt2):
        D = self.det(self.getA(), pt2.getA(), self.getB(), pt2.getB())
        Dx = self.det(-self.getC(), -pt2.getC(), self.getB(), pt2.getB())
        Dy = self.det(self.getA(), pt2.getA(), -self.getC(), -pt2.getC())
        if D == 0:
            return
        return (Dx * 1.0 / D, Dy * 1.0 / D)

    def __str__(self):
        return 'StraightLine({}x {} {}y {} {} = 0)'.format(self.getA(),
                                                           self.checkdau(self.getB()),
                                                           abs(self.getB()),
                                                           self.checkdau(self.getC())
                                                           , abs(self.getC()))
