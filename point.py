from math import hypot

class Point(object):
    """Creates a point on a coordinate plane with values x and y."""

    def __init__(self, x=0, y=0):
        """ Create a new point at x, y

        @:parameter:
        :x - float number (default 0)
        :y - float number (default 0)

        @:returns: new instance
        """
        self.x = float(x)
        self.y = float(y)

    @classmethod
    def create(cls, args):
        """ args.__class__.__name__ """
        if isinstance(args, str):
            return cls(*map(float, args[args.index('(') + 1:args.index(')')].split(',')))
        elif isinstance(args, (list, tuple)):
            return cls(*args)
        elif isinstance(args, dict):
            return cls(**args)
        elif isinstance(args, Point):
            return args
        else:
            raise ValueError("Invalid args, should be str, list, tuple, dict or instance of Point.")

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def to_list(self):
        return [self.x, self.y]

    def move(self, dx, dy):
        """Determines where x and y move

        @:param: dx The motion x
        @:param: dy The motion y
        """
        self.x = self.x + dx
        self.y = self.y + dy

    def midpoint(self, other):
        """
        @:param: point The 2nd point

        @:return The midpoint of points p1 and p2
        """
        mx = (self.x + other.x) / 2
        my = (self.y + other.y) / 2
        return Point(mx, my)

    def distance_from_origin(self):
        """
        @:return  A point and distance from origin
        """
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def distance(self, other):
        """
        @:param: point The 2nd point

        @:return Return a 2 point and distance
        """
        return hypot(other.x - self.x, other.y - self.y)

    def pointInspace(self, other):

        return

    def __str__(self):
        case = {
            1: {True: int(self.x), False: self.x},
            2: {True: int(self.y), False: self.y}}
        return 'Point(%s,%s)' % (case[1][self.x == int(self.x)],
                                 case[2][self.y == int(self.y)])
